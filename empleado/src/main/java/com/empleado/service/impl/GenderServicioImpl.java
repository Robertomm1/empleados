package com.empleado.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.empleado.service.entidades.Genders;
import com.empleado.service.repositorio.GenderRepositorio;
import com.empleado.service.servicio.GenderServicio;

@Service
public class GenderServicioImpl implements GenderServicio {

	@Autowired
	private GenderRepositorio empledaoServcio;

	@Override
	public List<Genders> listaGender() {
		return (List<Genders>) empledaoServcio.findAll();
	}

}
