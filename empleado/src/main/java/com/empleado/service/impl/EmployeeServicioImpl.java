package com.empleado.service.impl;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.empleado.service.entidades.DtoGeneric;
import com.empleado.service.entidades.Employees;
import com.empleado.service.entidades.Genders;
import com.empleado.service.entidades.Jobs;
import com.empleado.service.repositorio.EmpleadoRepositorio;
import com.empleado.service.repositorio.GenderRepositorio;
import com.empleado.service.repositorio.JobsRepositorio;
import com.empleado.service.response.ResponseEmployees;
import com.empleado.service.response.ResponseSaveEmployee;
import com.empleado.service.servicio.EmployeeServicio;

@Service
public class EmployeeServicioImpl implements EmployeeServicio {

	@Autowired
	private EmpleadoRepositorio empledaoRepositorio;

	@Autowired
	private GenderRepositorio genderRepositorio;

	@Autowired
	private JobsRepositorio jobRepositorio;

	static final int MAYOR = 18;

	@Override
	public ResponseSaveEmployee save(DtoGeneric empleado) {
		Employees usuario = new Employees();
		ResponseSaveEmployee response = new ResponseSaveEmployee();
		Employees user = new Employees();
		int edad = 0;
		edad = calcularEdad(empleado.getBirthdate());

		try {

			user = consultarEmpleado(empleado);

			if (user == null) {
				if (edad >= MAYOR) {
					Jobs job = jobRepositorio.getById(empleado.getJob_id());
					Genders gender = genderRepositorio.getById(empleado.getGender_id());

					usuario.setJob(job);
					usuario.setGender(gender);
					usuario.setLast_name(empleado.getLast_name());
					usuario.setName(empleado.getName());
					usuario.setBirthdate(empleado.getBirthdate());

					Employees id = empledaoRepositorio.save(usuario);
					response.setId(String.valueOf(id.getId()));
					response.setSuccess(true);
					return response;
				}
				response.setSuccess(false);
			} else {
				response.setSuccess(false);
			}

		} catch (Exception e) {
			response.setSuccess(false);
		}
		return response;

	}

	private int calcularEdad(Date birthdate) {
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate fechaNac = LocalDate.parse(String.valueOf(birthdate), fmt);
		LocalDate ahora = LocalDate.now();
		Period periodo = Period.between(fechaNac, ahora);

		return periodo.getYears();
	}

	private Employees consultarEmpleado(DtoGeneric empleado) {

		return empledaoRepositorio.findbyNameAndLastName(empleado.getName(), empleado.getLast_name());
	}

	@Override
	public ResponseEmployees getEmployees(DtoGeneric empleados) {
		ResponseEmployees response = new ResponseEmployees();
		List<Employees> emp = empledaoRepositorio.getEmpleadoJob(empleados.getJob_id());
		response.setEmployees(emp);
		if (emp.isEmpty()) {
			response.setSuccess(false);

		} else {
			response.setSuccess(true);
		}
		return response;
	}

}
