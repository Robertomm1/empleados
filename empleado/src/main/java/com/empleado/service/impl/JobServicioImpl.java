package com.empleado.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.empleado.service.entidades.Jobs;
import com.empleado.service.repositorio.JobsRepositorio;
import com.empleado.service.servicio.JobServicio;

@Service
public class JobServicioImpl implements JobServicio {

	@Autowired
	private JobsRepositorio jobServicio;

	@Override
	public List<Jobs> listaJob() {
		return (List<Jobs>) jobServicio.findAll();
	}

	@Override
	public List<Jobs> listaJobId() {

		return (List<Jobs>) jobServicio.findAllById(1);
	}

}
