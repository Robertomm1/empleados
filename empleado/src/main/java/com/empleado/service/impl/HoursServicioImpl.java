package com.empleado.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empleado.service.entidades.DtoGeneric;
import com.empleado.service.entidades.EmployeeWorkedHours;
import com.empleado.service.entidades.Employees;
import com.empleado.service.repositorio.EmpleadoRepositorio;
import com.empleado.service.repositorio.HoursRepositorio;
import com.empleado.service.response.ResponseHours;
import com.empleado.service.response.ResponsePago;
import com.empleado.service.response.ResponseSaveHours;
import com.empleado.service.servicio.HoursServicio;

@Service
public class HoursServicioImpl implements HoursServicio {

	@Autowired
	private HoursRepositorio hoursRepositorio;

	@Autowired
	private EmpleadoRepositorio empleadoRepositorio;

	static final int HORAS = 20;

	@Override
	public ResponseSaveHours save(DtoGeneric horas) {
		Employees usuario = new Employees();
		EmployeeWorkedHours hora = new EmployeeWorkedHours();
		ResponseSaveHours response = new ResponseSaveHours();

		try {

			usuario = consultarEmpleado(horas.getEmployee_id());
			LocalDate fecha = calcularFecha();
			DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate fechaTrabajo = LocalDate.parse(String.valueOf(horas.getWorked_date()), fmt);
			
			int hrsumas = 0;
			boolean registro = true;
			for (EmployeeWorkedHours hr : usuario.getEmployee_worked_hours()) {
				hrsumas += hr.getWorked_hours();
			}
			
			int total = hrsumas + horas.getWorked_hours();
			if(registro) {
				if (fechaTrabajo.isBefore(fecha) || fechaTrabajo.equals(fecha)) {
					if (total <= HORAS) {
						if (usuario != null) {
							hora.setEmployee(usuario);
							hora.setWorked_date(horas.getWorked_date());
							hora.setWorked_hours(horas.getWorked_hours());

							EmployeeWorkedHours id = hoursRepositorio.save(hora);
							response.setId(String.valueOf(id.getId()));
							response.setSuccess(true);
						} 
					} 
				}
			}
			
			 
		} catch (Exception e) {

			response.setSuccess(false);
		}
		return response;

	}

	private LocalDate calcularFecha() {
		LocalDate ahora = LocalDate.now();

		return ahora;
	}

	private Employees consultarEmpleado(int employee_id) {
		Employees user = empleadoRepositorio.getById(employee_id);
		return user;
	}

	@Override
	public ResponseHours getHours(DtoGeneric hora) {

		ResponseHours response = new ResponseHours();

		try {

			if (hora.getStart_date().before(hora.getEnd_date())) {
				List<EmployeeWorkedHours> suma = hoursRepositorio.countHours(hora.getEmployee_id(),
						hora.getStart_date(), hora.getEnd_date());
				int sum = 0;
				for (EmployeeWorkedHours h : suma) {
					sum += h.getWorked_hours();
				}

				response.setTotal_worked_hours(sum);
				response.setSuccess(true);
			} else {

				response.setSuccess(false);
			}

		} catch (Exception e) {
			response.setSuccess(false);
			return response;
		}

		return response;

	}

	@Override
	public ResponsePago getHoursPayment(DtoGeneric paga) {
		ResponsePago response = new ResponsePago();

		try {

			if (paga.getStart_date().before(paga.getEnd_date())) {
				List<EmployeeWorkedHours> suma = hoursRepositorio.countHours(paga.getEmployee_id(),
						paga.getStart_date(), paga.getEnd_date());
				int sum = 0;
				for (EmployeeWorkedHours h : suma) {
					sum += h.getWorked_hours();
				}
				System.out.println("horas"+sum); 
				response.setPaymnet(sum);
				response.setSuccess(true);
			} else {

				response.setSuccess(false);
			}

		} catch (Exception e) {
			response.setSuccess(false);
			return response;
		}

		return response;
	}

}
