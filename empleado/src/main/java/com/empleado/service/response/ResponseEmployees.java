package com.empleado.service.response;

import java.util.List;

import com.empleado.service.entidades.Employees;

public class ResponseEmployees {

	private boolean success;
	private List<Employees> Employees;

	public List<Employees> getEmployees() {
		return Employees;
	}

	public void setEmployees(List<Employees> employees) {
		Employees = employees;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
