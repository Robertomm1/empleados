package com.empleado.service.response;

public class ResponsePago {

	private int paymnet;
	private boolean success;

	public int getPaymnet() {
		return paymnet;
	}

	public void setPaymnet(int paymnet) {
		this.paymnet = paymnet;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
