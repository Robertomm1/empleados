package com.empleado.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.empleado.service.entidades.DtoGeneric;
import com.empleado.service.response.ResponseEmployees;
import com.empleado.service.response.ResponseHours;
import com.empleado.service.response.ResponsePago;
import com.empleado.service.response.ResponseSaveEmployee;
import com.empleado.service.response.ResponseSaveHours;
import com.empleado.service.servicio.EmployeeServicio;
import com.empleado.service.servicio.HoursServicio;

@RestController
public class EmpleadoControlador {

	@Autowired
	private EmployeeServicio empleadoServicio;

	@Autowired
	private HoursServicio hoursServicio;

	@PostMapping("/saveEmployee")
	public ResponseEntity<ResponseSaveEmployee> save(@RequestBody DtoGeneric empleado) {
		ResponseSaveEmployee id = empleadoServicio.save(empleado);
		return ResponseEntity.ok(id);
	}

	@PostMapping("/saveHours")
	public ResponseEntity<ResponseSaveHours> saveHours(@RequestBody DtoGeneric horas) {
		ResponseSaveHours id = hoursServicio.save(horas);
		return ResponseEntity.ok(id);
	}

	@PostMapping("/getEmployees")
	public ResponseEntity<ResponseEmployees> getEmployees(@RequestBody DtoGeneric empleados) {
		ResponseEmployees listaEmpleados = empleadoServicio.getEmployees(empleados);
		return ResponseEntity.ok(listaEmpleados);
	}

	@PostMapping("/getHours")
	public ResponseEntity<ResponseHours> getHours(@RequestBody DtoGeneric hora) {
		ResponseHours horas = hoursServicio.getHours(hora);
		return ResponseEntity.ok(horas);
	}

	@PostMapping("/getPayment")
	public ResponseEntity<ResponsePago> getPayment(@RequestBody DtoGeneric paga) {
		ResponsePago pago = hoursServicio.getHoursPayment(paga);
		return ResponseEntity.ok(pago);
	}

}
