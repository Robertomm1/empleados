package com.empleado.service.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.empleado.service.entidades.Genders;

@Repository
public interface GenderRepositorio extends JpaRepository<Genders, Integer> {

}
