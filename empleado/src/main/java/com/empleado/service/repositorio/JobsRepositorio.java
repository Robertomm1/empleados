package com.empleado.service.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.empleado.service.entidades.Jobs;

@Repository
public interface JobsRepositorio extends JpaRepository<Jobs, Integer> {

	List<Jobs> findAllById(int i);

}
