package com.empleado.service.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.empleado.service.entidades.Employees;

@Repository
public interface EmpleadoRepositorio extends JpaRepository<Employees, Integer> {

	@Query(value = "SELECT e.id, e.birthdate, e.last_name, e.NAME,e.gender_id,e.job_id, j.id, j.name, j.salary FROM employee e INNER JOIN Job j ON e.job_id = j.id WHERE e.job_id = :id", nativeQuery = true)
	public List<Employees> getEmpleadoJob(int id);

	@Query(value = "SELECT e.id, e.birthdate, e.last_name, e.NAME, e.gender_id, e.job_id FROM employee e WHERE e.name = :nombre AND e.last_name = :apellido", nativeQuery = true)
	public Employees findbyNameAndLastName(String nombre, String apellido);

}
