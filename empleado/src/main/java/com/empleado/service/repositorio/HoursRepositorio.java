package com.empleado.service.repositorio;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.empleado.service.entidades.EmployeeWorkedHours;

@Repository
public interface HoursRepositorio extends JpaRepository<EmployeeWorkedHours, Integer> {

	List<EmployeeWorkedHours> findByEmployeeId(int i);

	@Query(value = "SELECT h.id,h.worked_date, h.worked_hours, h.employee_id FROM employee_worked_hours h  where h.employee_id = :empleado AND worked_date BETWEEN :fechaI AND :fechaF", nativeQuery = true)
	public List<EmployeeWorkedHours> countHours(int empleado, Date fechaI, Date fechaF);

}
