package com.empleado.service.servicio;

import com.empleado.service.entidades.DtoGeneric;
import com.empleado.service.response.ResponseHours;
import com.empleado.service.response.ResponsePago;
import com.empleado.service.response.ResponseSaveHours;

public interface HoursServicio {

	public abstract ResponseSaveHours save(DtoGeneric horas);

	public abstract ResponseHours getHours(DtoGeneric hora);

	public abstract ResponsePago getHoursPayment(DtoGeneric paga);
}
