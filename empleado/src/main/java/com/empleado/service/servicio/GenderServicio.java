package com.empleado.service.servicio;

import java.util.List;
import com.empleado.service.entidades.Genders;

public interface GenderServicio {

	public abstract List<Genders> listaGender();
}
