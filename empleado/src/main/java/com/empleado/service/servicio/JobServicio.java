package com.empleado.service.servicio;

import java.util.List;

import com.empleado.service.entidades.Jobs;

public interface JobServicio {

	public abstract List<Jobs> listaJob();

	public abstract List<Jobs> listaJobId();
}
