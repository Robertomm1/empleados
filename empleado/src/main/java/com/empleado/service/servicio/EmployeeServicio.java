package com.empleado.service.servicio;

import com.empleado.service.entidades.DtoGeneric;
import com.empleado.service.response.ResponseEmployees;
import com.empleado.service.response.ResponseSaveEmployee;

public interface EmployeeServicio {

	public abstract ResponseSaveEmployee save(DtoGeneric empleado);

	public abstract ResponseEmployees getEmployees(DtoGeneric empleados);

}
